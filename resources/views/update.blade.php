@if(Session::has('success'))
  {{Session::get('success')}}
@else
<form action="{{route('Save')}}" method="post" enctype="multipart/form-data">
  {{csrf_field()}}
  <input type="text" name="id" value="{{$user->id}}" hidden>
  <input type="text" name="fullname" value="{{$user->name}}" placeholder="Full nameee">
  <input type="email" name="email" value="{{$user->email}}" placeholder="e-mail">
  <input type="phone" name="phone" value="{{$user->phone}}">
  <input type="text" name="login" value="{{$user->login}}">
  <input type="password" name="password" value="{{$user->password}}">
  <input type="submit" value="Registrate">
</form>
@endif
