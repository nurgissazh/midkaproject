<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
Use Illuminate\Database\Eloquent\Collection;

class AdminController extends Controller
{
  public function reg(Request $req){
    $user = new User();
    $user->name = $req['fullname'];
    $user->email = $req['email'];
    $user->phone = $req['phone'];
    $user->login = $req['login'];
    $user->password = $req['password'];
    $user->save();
    return back()->with('success', 'You have been sign up');
  }
  public function delete($id){
    $user = User::find($id);
    $user->delete();
    return back()->with('success', 'You have deleted');
  }
  public function update($id){
    $user = User::find($id);
    return view('update', compact(['user']));
  }
  public function save(Request $req){
    $user = User::find($req['id']);
    $user->name = $req['fullname'];
    $user->email = $req['email'];
    $user->phone = $req['phone'];
    $user->login = $req['login'];
    $user->password = $req['password'];
    $user->save();
    return back()->with('success', 'You have updated the text');
  }
}
