<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'main',
    'uses'=>'Controller@index'
    ]);
Route::post('/reg', 'AdminController@reg')->name('reg');
Route::get('/delete/{id}', 'AdminController@Delete')->name('Delete');
Route::get('/update/{id}', 'AdminController@Update')->name('Update');
Route::post('/save', 'AdminController@Save')->name('Save');
